#!/bin/bash
farm_spacecrypto() {
  now=$(date)
  echo "\n\n[$now]======================= starting spacecrypto"

  cd $base_dir/spacecrypto-bot
  private_keys=($(awk -F= '{print $1}' .keys))
  accounts_ran=0
  ((direction^=1)) #flips direction everytime
  i_start=$((0 + $direction * 2))
  for (( i=$i_start; i<$i_start+2; i++))
  do
    export ACCOUNT_PRIVATE_KEY=${private_keys[$i]}
    export BOT_WINDOW_LEFT=$(($accounts_ran * 1000))
    export BOT_WINDOW_TOP=800
    export BOT_WINDOW_WIDTH=1200
    export BOT_WINDOW_HEIGHT=800

    account_prefix=${ACCOUNT_PRIVATE_KEY:0:8}
    now=$(date)
    echo "\n\n\n[$now]======================================================"
    echo "RUNNING FOR PRIVATE_KEY STARTING IN $account_prefix"

    PID=`cat .${account_prefix}.pid`
    echo "checking pid $PID. if its running, do nothing"
    ps -p $PID > /dev/null && ! grep -q FATALERRORIMDYING ".$account_prefix.logs" && continue
    echo "killing ${PID}" && kill -9 $PID
    echo "starting game"
    # clean_modules 
    
    nohup node main.js > ".$account_prefix.logs" &
    PID=`echo $!` && echo $PID > ".${account_prefix}.pid"
    i=$(($i+1))
    accounts_ran=$(($accounts_ran+1))
    j=0
    limit=60 # 10 minutes in seconds divided per 5
    while [ $j -lt $limit ];
    do
      j=$(($j+1))
      sleep 5
      grep -q FARMSTARTED ".$account_prefix.logs" && break
    done
  done
}

while [ 1=1 ]
do

  farm_spacecrypto

  i=$(($i+1))
  sleep_for=$(( RANDOM % 6 ))
  now=$(date)
  then=$(date -v+${sleep_for}S)
  echo "[$now] sleeping for $sleep_for seconds. check back at ${then}. i=$i"
  sleep $sleep_for
done