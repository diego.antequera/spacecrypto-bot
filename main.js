const { Browser } = require('./src/browser')
const { Metamask } = require('./src/metamask.js')

const defaultPassword = 'os8aduaho641'
const bscNetworkInfo = {
  name: 'Binance Smart Chain Mainnet',
  rpcUrl: 'https://bsc-dataseed.binance.org/',
  chainId: '56',
  currencySymbol: 'BNB',
  blockExplorer: 'https://bscscan.com/',
}
const tokenContractAddress = '0x0ecaf010fc192e2d5cbeb4dfb1fee20fbd733aa1'
const tokenSymbol = 'SPG'
const baseUrl = 'https://play.spacecrypto.io/'
doRetry = true
const SUFFIX = process.env.ACCOUNT_PRIVATE_KEY.substring(0, 8)

class IvoryTower {
  constructor() {
    this.doRetry = true
    this._browser = new Browser()
  }

  get metamask() {
    return this._metamask
  }

  get browser() {
    return this._browser
  }

  get gamePage() {
    return this._gamePage
  }

  async setupWallet() {
    console.log('-- setup wallet')
    const metamaskPage = await this.browser.getPage('#initialize')

    console.log('--- Initializing Metamask')
    const metamask = new Metamask(metamaskPage)
    const url = await metamaskPage.url()
    await metamask.initializeMetamask(defaultPassword)
    console.log('Adding network', bscNetworkInfo)
    await metamask.addNetwork(bscNetworkInfo)
    await metamask.importPrivateKey(process.env.ACCOUNT_PRIVATE_KEY)
    await metamask.addToken(tokenSymbol, tokenContractAddress)
    await metamaskPage.goto(url)
    await metamaskPage.waitForTimeout(2000)
    this._metamask = metamask
    console.log('-- done')
  }

  async setupGame() {
    console.log('-- setup game components')
    if (this.gamePage)
      return this.login()

    console.log('--- opening main page')
    this._gamePage = await this.browser.getNewPage(baseUrl)
    console.log('--- connecting account')
    console.log('--- clicking connect wallet button')
    await this.gamePage.waitForImageAndClick('btn_connectWallet', { timeout: 180000 })
    await this.gamePage.waitForTimeout(1000)
    console.log('--- searching for a connect window now')
    let connectAccountPopup = await this.browser.getPage('notification.html#connect')
    console.log('--- found it! connecting account')
    await this.metamask.connectAccount(connectAccountPopup)
    await this.gamePage.waitForTimeout(1000)
    console.log('--- searching for a signin window now')
    let signinPopup = await this.browser.getPage('signature-request')
    console.log('--- found it! signing in')
    await this.metamask.signIn(signinPopup)
    await this.gamePage.waitForTimeout(1000)
    console.log('--- clicking play button')
    await this.gamePage.waitForImageAndClick('btn_play')
    console.log('-- done')
  }

  async login() {
    console.log('--- login')
    await this.gamePage.waitForTimeout(10000)
    console.log('--- clicking connect wallet button')
    await this.gamePage.waitForImageAndClick('btn_connectWallet')
    console.log('--- searching for a signin window now')
    let signinPopup = await this.browser.getPage('signature-request')
    console.log('--- found it! signing in')
    await this.metamask.signIn(signinPopup)
    await this.gamePage.waitForTimeout(1000)
    console.log('--- clicking play button')
    await this.gamePage.waitForImageAndClick('btn_play')
    console.log('-- done')
  }

  async close() {
    if (this.browser)
      await this.browser.close()
  }

  async run() {
    try {
      console.log('- run')
      if (!this.metamask)
        await this.setupWallet()

      if (!this.gamePage)
        await this.setupGame()
      else
        await this.login()
      console.log('- all setups done, waiting for it to load')
      await this.gamePage.waitForImage('btn_fightBoss')

      console.log('- farming')
      await this.farm()

      console.log('- ran! [CODE_COMPLETED_MOVE_TO_NEXT]')
    }
    catch (e) {
      // reload something goes wrong
      console.log('oops somethings wrong', e)
      if (this.gamePage && await this.gamePage.findImage('lbl_processing')) {
        console.log('FATALERROR - processing')
        return this.restart()

      }

      console.log('error! retrying...', { e })
      return this.whereAmI()
    }
  }

  async whereAmI(attempts = 0) {
    console.log('-- trying to figure out where I am!')
    if (!this.gamePage) {
      console.log('-- no gamepage, no nothing, restart')
      return this.restart()
    }
    else if (await this.gamePage.findImage('btn_connectWallet')) {
      console.log('-- connect wallet stuff')
      return this.restart()
    }
    else if (await this.gamePage.findImage('btn_fightBoss')) {
      console.log('-- in lobby, lets farm')
      return this.farm()
    }
    else if (await this.gamePage.findImage('btn_surrender')) {
      console.log('-- in game! just wait')
      return this.waitForBattleEnd()
    }
    else if (await this.gamePage.findImage('btn_confirm')) {
      console.log('-- in game! just wait')
      return this.waitForBattleEnd()
    }
    else if (attempts > 10) {
      console.log('-- I give up... restarting it all')
      return this.restart()
    }
    console.log('-- I dont know! lets wait a bit and try again')
    await this.gamePage.waitForTimeout(1000)
    return this.whereAmI(attempts + 1)
  }

  async restart() {
    console.log('restarting all this shit...')
    await this.close()
    getRich()
  }

  async farm() {
    // await this.removeAllShip()
    if (!await this.gamePage.findImage('lbl_15shipsInBattle',
      {
        useCache: false,
        confidence: 0.97
      })) {
      console.log(`-- need to add more ships`)
      await this.orderByMaxAmmo()
      console.log(`-- adding ships to battle until we have 15 ships`)
      await this.selectUntilFull(15)
    }
    console.log(`-- starting fight`)
    await this.gamePage.clickOnImage('btn_fightBoss')
    console.log(`-- checking for confirmation button`)
    const imgCoords = await this.gamePage.waitForImage('btn_confirm')
    if (imgCoords) {
      await this.gamePage.clickOnCoords(imgCoords)
    }
    console.log('FARMSTARTED')
    console.log('-- chilling for 30 min')
    await this.gamePage.waitForTimeout(15 * 60 * 1000)
    return this.waitForBattleEnd()
  }

  async removeAllShip() {
    console.log('-- removing all ships')
    try {
      await this.gamePage.clickOnImage('btn_removeShip', {
        useCache: false
      })
      console.log('-- deselected')
      return this.removeAllShip()
    }
    catch (e) {
      console.log('-- done!')
      return true
    }
  }

  async orderByMaxAmmo() {
    console.log('-- ordering by max ammo')
    if (await this.gamePage.findImage('btn_orderMaxAmmo')) {
      console.log('-- already ordered!')
      return true
    }
    let imgCoords = await this.gamePage.findImage('btn_orderNewest')
    console.log('-- trying to find "newest"')
    if (!imgCoords) {
      console.log('--- no good, maybe "min ammo?"')
      imgCoords = await this.gamePage.findImage('btn_orderMinAmmo')
    }
    if (!imgCoords) {
      console.log('--- fuck im lost -- cant find any order, but I wont break stuff')
      return false
    }
    await this.gamePage.clickOnCoords(imgCoords)
    console.log('-- ordering now')
    await this.gamePage.waitForImageAndClick('btn_orderOptionMaxAmmo')
    console.log('-- done!')
    return true
  }

  async repairShips() {
    console.log('-- repairing ships -- for some reason, this doesnt work, so skipping')
    return true
    try {
      console.log('--- trying to repair')
      await this.gamePage.clickOnImage('btn_repairShip', {
        useCache: false
      })
      console.log('--- confirming...')
      await this.gamePage.waitForImageAndClick('btn_yes')
      console.log('--- repaired')
      return this.repairShips()
    }
    catch (e) {
      console.log('-- done!')
      return this.selectUntilFull()
    }

  }

  async selectUntilFull(thisFull = 15, added = 0) {
    try {
      if (added >= thisFull && await this.gamePage.findImage('lbl_15shipsInBattle',
        {
          useCache: false,
          confidence: 0.97
        })) {
        console.log('-- done! all ships added')
        return true
      }
      console.log('-- trying to add ship...')
      await this.gamePage.clickOnImage('btn_addShipToFight', {
        useCache: false,
        confidence: 0.92
      })
      console.log(`-- added ${added}/${thisFull}`)
      added += 1
    }
    catch (e) {
      if (await this.gamePage.findImage('lbl_15shipsInBattle',
        {
          useCache: false,
          confidence: 0.97
        })) {
        console.log('-- done! all ships added')
        return true
      }

      if (await this.gamePage.findImage('btn_repairShip',
        {
          useCache: false
        })) {
        return await this.repairShips()
      }

      console.log('-- couldnt add now, lets wait some and then try again...')
      await this.gamePage.waitForTimeout(60000)
      return this.whereAmI()
    }
    return this.selectUntilFull(thisFull, added)
  }

  async waitForBattleEnd() {
    console.log('-- waiting for battle to end...')
    const imgCoords = await this.gamePage.waitForImage('btn_confirm', {
      timeout: 1800000, // half hour
      pollingRate: 5000
    })
    if (imgCoords) {
      console.log('-- it ended! ending process')
      return this.close()
    }
    return this.waitForBattleEnd()
  }
}

async function getRich() {
  const tower = new IvoryTower()
  try {
    await tower.run()
  }
  catch (e) {
    console.log('error!', { e })
    await tower.close()
    return getRich()
  }
}

getRich()
// console.log(`it should be running this ${process.env.BCOIN_ACCOUNT}`)
