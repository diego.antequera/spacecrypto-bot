const puppeteer = require('puppeteer');
const { Region, mouse, straightTo, centerOf, screen, imageResource, randomPointIn, OptionalSearchParameters } = require("@nut-tree/nut-js")
require("@nut-tree/template-matcher")

const SUFFIX = process.env.ACCOUNT_PRIVATE_KEY.substring(0, 8)

function expandPage(page) {
  page.__proto__.typeText = typeText
  page.__proto__.waitAndClick = waitAndClick
  page.__proto__.clickIfExists = clickIfExists
  page.__proto__.clickUntilSelector = clickUntilSelector
  page.__proto__.exists = exists
  page.__proto__.innerText = innerText
  page.__proto__.waitUntilSelectorGone = waitUntilSelectorGone
  page.__proto__.resetValue = resetValue
  page.__proto__.gotoUrlIfDiferent = gotoUrlIfDiferent
  page.__proto__.setValue = setValue
  page.__proto__.getValue = getValue
  page.__proto__.findImage = findImage
  page.__proto__.waitForImage = waitForImage
  page.__proto__.clickOnImage = clickOnImage
  page.__proto__.clickOnCoords = clickOnCoords
  page.__proto__.waitForImageAndClick = waitForImageAndClick
}

async function typeText(selector, value, delay = 1) {
  await this.focus(selector)
  await this.keyboard.type(value, { delay: delay })
}

async function waitAndClick(waitSelector, clickSelector, timeout = 5000) {
  if (!clickSelector) {
    clickSelector = waitSelector
  }
  await this.waitForSelector(waitSelector, { timeout: timeout })
  await this.click(clickSelector)
}

async function clickIfExists(selector) {
  try {
    if (this.exists(selector)) {
      this.click(selector)
    }
  }
  catch (e) {
    console.log('selector doesnt exist, doing nothing')
  }
}

async function exists(selector, xpath = false) {
  try {
    if (xpath) {
      return (await this.$x(selector)) !== []
    }
    return (await this.$(selector)) !== null
  }
  catch (e) {
    return false
  }
}

async function clickUntilSelector(clickSelector, conditionalSelector) {
  await this.waitAndClick(clickSelector)
  await this.waitForTimeout(1000)
  if (!await this.exists(conditionalSelector)) {
    return await this.clickUntilSelector(clickSelector, conditionalSelector)
  }
}

async function innerText(selector) {
  return await this.$eval(selector, e => e.innerText)
}

async function resetValue(selector) {
  return await this.$eval(selector, e => e.value = '')
}

async function setValue(selector, value) {
  return await this.$eval(selector, (e, v) => e.value = v, value)
}

async function getValue(selector) {
  return await this.$eval(selector, (e) => e.value)
}

async function waitUntilSelectorGone(selector) {
  if (await this.exists(selector)) {
    await this.waitForTimeout(1000)
    return this.waitUntilSelectorGone(selector)
  }
  return true
}

async function gotoUrlIfDiferent(url) {
  let currentUrl = await this.url()
  if (currentUrl !== url) {
    try {
      await this.goto(url, { waitUntil: 'networkidle2' })
    }
    catch (e) {
      if (e instanceof puppeteer.errors.TimeoutError) {
        console.log('timeout error, trying again', e)
        return this.gotoUrlIfDiferent(url)
      }
      throw e
    }
  }
}

var lastImageName = null
var lastImageCoords = null
async function findImage(needleName, optionsParam) {
  const img = imageResource(`/models/${needleName}.png`)
  const options = getDefaultOptions(optionsParam)
  const { useCache } = options
  await this.bringToFront()
  if (useCache && needleName == lastImageName)
    return lastImageCoords
  try {
    const scr = await screen.find(img, await getImageSearchParameters(optionsParam))
    lastImageName = needleName
    lastImageCoords = scr
    return scr
  }
  catch (e) {
    console.log(e)
    return false
  }
}

async function waitForImage(needleName, optionsParam) {
  const options = getDefaultOptions(optionsParam)
  const { pollingRate, timeout } = options
  const img = imageResource(`/models/${needleName}.png`)
  await this.bringToFront()
  try {
    const scr = await screen.waitFor(img, timeout, pollingRate, await getImageSearchParameters(optionsParam))
    lastImageName = needleName
    lastImageCoords = scr
    return scr
  }
  catch (e) {
    console.log(e)
    return false
  }
}

async function waitForImageAndClick(imageName, optionsParam = {}) {
  const imageCoords = await this.waitForImage(imageName, optionsParam)
  if (!imageCoords) {
    throw new Error(`cant find image '${imageName}'`)
  }
  await this.clickOnCoords(imageCoords)
}

async function clickOnImage(imageName, optionsParam = {}) {
  const imageCoords = await this.findImage(imageName, optionsParam)
  if (!imageCoords) {
    throw new Error(`cant find image '${imageName}'`)
  }
  await this.clickOnCoords(imageCoords)
}

async function clickOnCoords(coords) {
  await mouse.move(straightTo(randomPointIn(coords)))
  await this.bringToFront()
  await mouse.leftClick()
  return coords
}

function getDefaultOptions(options = {}) {
  return {
    pollingRate: options.pollingRate || 1000,
    timeout: options.timeout || 45000,
    useCache: options.useCache !== undefined ? options.useCache : true
  }
}

async function getImageSearchParameters(options = {}) {
  const region = new Region(parseInt(process.env.BOT_WINDOW_LEFT || 1200), parseInt(process.env.BOT_WINDOW_TOP || 0), parseInt(process.env.BOT_WINDOW_WIDTH || 1200), parseInt(process.env.BOT_WINDOW_HEIGHT || 800))
  return new OptionalSearchParameters(
    region,
    options.confidence || 0.98,
    options.scaleImages !== undefined ? options.scaleImages : true
  )
}

module.exports = { expandPage }